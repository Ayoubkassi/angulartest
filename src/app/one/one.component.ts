import { Component, OnInit } from '@angular/core';
import { StateService } from '../state.service';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.scss']
})
export class OneComponent implements OnInit {
  checkBoxStateOne : boolean = false;


  constructor(public stateService:StateService) { }

  ngOnInit(): void {
     this.stateService.reusableComponentState.subscribe( checkBoxStateOne => {
       this.checkBoxStateOne = checkBoxStateOne;
   });

  }

}
